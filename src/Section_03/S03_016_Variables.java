package Section_03;

public class S03_016_Variables {

    public static void main(String[] args) {

        String name = "Ian";
        System.out.println("Hello, World! and Hello " + name + "!");

        int myFirstNumber = (10 + 5) + (2 * 10);
        int mySecondNumber = 12;
        int myThirdNumber = myFirstNumber * 2;

        // Note: string concatination due to string casting prior to addition
        System.out.println("Sum1: " + myFirstNumber + mySecondNumber + myThirdNumber);

        // In this case addition is performed first
        System.out.println("Sum2: " + (myFirstNumber + mySecondNumber + myThirdNumber));

        int myTotal = myFirstNumber + mySecondNumber + myThirdNumber;
        System.out.println("myTotal: " + myTotal);

        int myDiff = 1000 - myTotal;
        System.out.println("myDiff: " + myDiff);


    }
}
