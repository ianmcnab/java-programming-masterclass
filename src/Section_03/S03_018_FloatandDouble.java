package Section_03;

public class S03_018_FloatandDouble {

    public static void main(String[] args) {

        // Int      32  - 4 bytes
        // Float    32  - 4 bytes
        // Double   64  - 8 bytes

        int myIntValue = 5 / 3;     // Integer division rounds down
        float myFloatValue = 5f / 3f;
        double myDoubleValue = 5d / 3d;

        System.out.println("myIntValue = " + myIntValue);
        System.out.println("myFloatValue = " + myFloatValue);
        System.out.println("myDoubleValue = " + myDoubleValue);

        // Convert a given number of pounds to kilograms
        // 1. Create a variable to store the number of pounds
        double numPounds = 200d;

        // 2. Calculate the number of Kilograms for the number above and store in a variable.
        double numKilograms = numPounds * 0.45359237d;

        // 3. Print out the result.
        System.out.println(numPounds + " pounds is " + numKilograms + " kilograms.");

        // NOTES: 1 pound is equal to 0.45359237 kilograms


    }
}
