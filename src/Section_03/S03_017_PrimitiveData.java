package Section_03;

public class S03_017_PrimitiveData {

    public static void main(String[] args) {

        // int has a width of 32
        int myMinValue = -2147483648;
        int myMaxValue =  2147483647;

        int myTotal = (myMinValue/2);

        // byte has a width of 8
        byte myByteValue = -128;        // -128 to 127

        // need to cast the 2 to a byte to prevent default int casting creating syntax error
        byte myNewByteValue = (byte) (myByteValue/2);

        // short has a width of 16
        short myShortValue = 32767;     // -32768 to 32767
        short myNewShortValue = (short) (myShortValue / 2 );

        // long has a width of 64  2^63
        long myLongValue = 9_223_372_036_854_775_807L;


        // 1. Create a byte variable and set it to any byte number
        byte b = 5;
        // 2. Create a short variable and set it to any valid short number
        short s = 6;
        // 3. Create an int variable and set it to any valid int number
        int i = 7;
        // 4 Create a variable of type long and make it equal to 5000 + 10 times the sum of the byte,
        //   plus the short plus the int
        // Note: long casts are optional as the long is larger than the other data type
        long l = 5000L + 10L * (long) (b + s + i);
        System.out.println("the total is: " + l);
    }


    }
