package Section_03;

public class S03_019_CharAndBoolean {

    public static void main(String[] args) {
        char myChar = 'A';  //
        char myCopyright = '\u00A9';    // 2 bytes
        System.out.println("Unicode output was: " + myCopyright);

        boolean myBool = true;
        boolean myFalse = false;

        // 1. Find the code for the registered symbol on the same line as the copyright symbol.
        // 2. Create a variable of type char and assign it the unicode value for that symbol.
        // 3. Display it on screen.

        // code is 00AE
       char registered = '\u00AE';
       System.out.println("Registered symbol is: " + registered);


    }
}
