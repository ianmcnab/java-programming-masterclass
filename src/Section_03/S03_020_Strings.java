package Section_03;

public class S03_020_Strings {

    public static void main(String[] args) {
        // Java Primitive types:
        // byte
        // short
        // int
        // long
        // float
        // double
        // char
        // boolean

        String s = "This is a string";
        System.out.println("s is equal to: " + s);
        s = s + ", and this is more.";
        System.out.println("s is equal to: " + s);
        s += " \u00A9 2015";
        System.out.println("s is equal to: " + s);

        String numberString = "250.55";
        numberString += "49.55";
        System.out.println(numberString);

        String lastString = "10";
        int myInt = 50;
        // implicit string cast on int
        lastString = lastString + myInt;
        // Cant' cast string as int, need to use an Integer class function to parse the value back to int
        myInt = myInt + Integer.parseInt(lastString);

        System.out.println("laststring is: " + lastString);
        System.out.println("myInt is: " + myInt);

        double doubleNumber = 120.47d;
        lastString = lastString + doubleNumber;
        System.out.println("Laststring value:" + lastString);
    }
}
